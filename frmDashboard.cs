﻿/*
 * Copyright 2017, 2018 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace Hire_Car_Maintenance_Inc {
    public partial class frmDashboard : Form {

        private static ArrayList companies = new ArrayList();
        private string dataFile = "input.txt";
        // https://bytes.com/topic/c-sharp/answers/258507-possible-disable-events
        // updateDisplay is used to stop execution of code while an object is being created.
        // Specifically, SelectedIndexChanged events are terminated early, to prevent exceptions.
        private bool updateDisplay = true;

        // Load events.

        public frmDashboard() {
            InitializeComponent();
        }

        private void frmDashboard_Load(object sender, EventArgs e) {
            ReadCompanyAndCarDataFromFile(companies);
            // The SelectedIndexChanged event will fire at the end of
            // the Display methods, prompting the GroupBox controls to fill.
            DisplayCompanies(companies);
            Company selectedCompany = GetSelectedCompany(companies, lstCompanies);
            DisplayCars(selectedCompany.Cars);
            // EnableSavingIfDetailsAreFilled() will enable save buttons on load, 
            // so they are manually disabled here.
            btnSaveCompanyDetails.Enabled = false;
            btnSaveCarDetails.Enabled = false;
        }

        // Event handlers.

        private void btnAddCompany_Click(object sender, EventArgs e) {
            DialogResult addCompany = ShowYesNoDialog("Are you sure you want to add a new company?", "Add new company");
            if (addCompany == DialogResult.Yes) {
                PrepareFormForCompanyCreation();
            }
        }

        private void btnAddCar_Click(object sender, EventArgs e) {
            string message = string.Format("Are you sure you want to add a new car to {0}?", txtName.Text);
            DialogResult addCar = ShowYesNoDialog(message, "Add new car");
            if (addCar == DialogResult.Yes) {
                PrepareFormForCarCreation();
            }
        }

        private void btnRemoveCar_Click(object sender, EventArgs e) {
            string message = string.Format("Are you sure you want to remove {0}?", txtMakeAndModel.Text);
            DialogResult removeCar = ShowYesNoDialog(message, "Remove car");
            if (removeCar == DialogResult.Yes) {
                Company selectedCompany = GetSelectedCompany(companies, lstCompanies);
                int carId = GetSelectedCarId(lstCars.SelectedItem.ToString());
                int selectedCarIndex = GetSelectedCarIndex(selectedCompany.Cars, carId);
                RemoveCar(selectedCompany, selectedCarIndex);
            }
        }

        private void btnSaveCompanyDetails_Click(object sender, EventArgs e) {
            string message;

            if (updateDisplay == false) {
                message = string.Format("Add {0} as a new company?", txtName.Text);
                DialogResult addCompany = ShowYesNoDialog(message, "Add new company");
                if (addCompany == DialogResult.Yes) {
                    AddNewCompany();
                }
                // If editing an existing company.
            } else {
                message = string.Format("Save the changes to {0}?", txtName.Text);
                DialogResult saveChanges = ShowYesNoDialog(message, "Save changes");
                if (saveChanges == DialogResult.Yes) {
                    // BUG: If searching for a car, lstCars will be re-drawn and show all cars.
                    Company selectedCompany = GetSelectedCompany(companies, lstCompanies);
                    EditExistingCompany(selectedCompany);
                }
            }
        }

        private void btnCancelCompanyCreation_Click(object sender, EventArgs e) {
            CancelObjectCreation(btnCancelCompanyCreation);
        }

        private void btnSaveCarDetails_Click(object sender, EventArgs e) {
            string message;

            if (updateDisplay == false) {
                message = string.Format("Add {0} as a new car?", txtMakeAndModel.Text);
                DialogResult addCar = ShowYesNoDialog(message, "Add new car");
                if (addCar == DialogResult.Yes) {
                    Company selectedCompany = GetSelectedCompany(companies, lstCompanies);
                    AddNewCar(selectedCompany);
                }
                // If editing an existing car.
            } else {
                message = string.Format("Save the changes to {0}?", txtMakeAndModel.Text);
                DialogResult saveChanges = ShowYesNoDialog(message, "Save changes");
                if (saveChanges == DialogResult.Yes) {
                    Company selectedCompany = GetSelectedCompany(companies, lstCompanies);
                    Car selectedCar = GetSelectedCar(selectedCompany.Cars, lstCars);
                    EditExistingCar(selectedCar, selectedCompany);
                }
            }
        }

        private void btnCancelCarCreation_Click(object sender, EventArgs e) {
            CancelObjectCreation(btnCancelCarCreation);
        }

        private void lstCompanies_SelectedIndexChanged(object sender, EventArgs e) {
            // When a ListBox item is changed, the SelectedIndexChanged event will fire twice:
            // first as -1, second as the actual SelectedIndex.
            // When the first event fires, code execution is terminated to prevent exceptions.
            if (lstCompanies.SelectedIndex == -1) {
                return;
            } else if (updateDisplay == true) {
                Company selectedCompany = GetSelectedCompany(companies, lstCompanies);
                DisplayCompanyDetails(selectedCompany);

                // When selecting an empty company, do not request Cars.
                if (selectedCompany.NumberOfCars == 0) {
                    DisplayCars(selectedCompany.Cars, -1);
                    EmptyTextBoxText(grpCarDetails.Controls);
                } else {
                    DisplayCars(selectedCompany.Cars);
                }
            }
        }

        private void lstCars_SelectedIndexChanged(object sender, EventArgs e) {
            Company selectedCompany = GetSelectedCompany(companies, lstCompanies);
            Car selectedCar = GetSelectedCar(selectedCompany.Cars, lstCars);
            DisplayCarDetails(selectedCar);
        }

        private void txtSearchCompanies_KeyUp(object sender, KeyEventArgs e) {
            List<string> searchResults = GetCompanySearchResults(companies, txtSearchCompanies.Text);

            if (searchResults.Count > 0) {
                AddItemsToListbox(lstCompanies, searchResults);
            } else if (searchResults.Count == 0) {
                lstCompanies.Items.Clear();
                EmptyTextBoxText(grpCompanyDetails.Controls);
                EmptyTextBoxText(grpCars.Controls);
                EmptyTextBoxText(grpCarDetails.Controls);
                lstCars.Items.Clear();
            } else {
                DisplayCompanies(companies);
            }
        }

        private void txtSearchCars_KeyUp(object sender, KeyEventArgs e) {
            Company company = GetSelectedCompany(companies, lstCompanies);
            List<string> searchResults = GetCarSearchResults(company.Cars, txtSearchCars.Text);

            if (searchResults.Count > 0) {
                AddItemsToListbox(lstCars, searchResults);
            } else if (searchResults.Count == 0) {
                lstCars.Items.Clear();
                EmptyTextBoxText(grpCarDetails.Controls);
            } else {
                DisplayCars(company.Cars);
            }
        }

        // Multiple event handlers.

        private void mtxCompanyId_KeyUp(object sender, KeyEventArgs e) {
            EnableSavingIfDetailsAreFilled(e, btnSaveCompanyDetails);
        }

        private void mtxCarId_KeyUp(object sender, KeyEventArgs e) {
            EnableSavingIfDetailsAreFilled(e, btnSaveCarDetails);
        }

        // Methods.

        private void DisplayCompanyDetails(Company company) {
            mtxCompanyId.Text = company.Id.ToString();
            txtName.Text = company.Name;
            txtAddress.Text = company.Address;
            txtPostcode.Text = company.Postcode;
            mtxNumberOfCars.Text = company.NumberOfCars.ToString();
        }

        private void DisplayCarDetails(Car car) {
            mtxCarId.Text = car.Id.ToString();
            txtMakeAndModel.Text = car.MakeAndModel;
            txtRegistration.Text = car.Registration;
            txtFuelType.Text = car.FuelType;
            txtLastServiceDate.Text = car.LastServiceDate;
            rtfComments.Text = car.Comments;
        }

        private void ReadCompanyAndCarDataFromFile(ArrayList listOfCompanies) {
            StreamReader streamReader = new StreamReader(dataFile);

            do {
                Company company = ReadCompanyFromFile(ref streamReader);
                listOfCompanies.Add(company);
                for (int i = 0; i < company.NumberOfCars; i++) {
                    Car car = ReadCarFromFile(ref streamReader, ref company);
                    company.Cars.Add(car);
                }
            } while (!streamReader.EndOfStream);

            streamReader.Close();
        }

        private void WriteCompanyAndCarDataToFile(ArrayList listOfCompanies) {
            StreamWriter streamWriter = new StreamWriter(dataFile);

            foreach(Company company in listOfCompanies) {
                streamWriter.Write(company.Information());
                for (int i = 0; i < company.NumberOfCars; i++) {
                    Car car = (Car) company.Cars[i];
                    streamWriter.Write(car.Information());
                }
            }

            streamWriter.Close();
        }

        private Company ReadCompanyFromFile(ref StreamReader streamReader) {
            int id = int.Parse(streamReader.ReadLine());
            string name = streamReader.ReadLine();
            string address = streamReader.ReadLine();
            string postcode = streamReader.ReadLine();
            int numberOfCars = int.Parse(streamReader.ReadLine());

            Company company = new Company(id, name, address, postcode, numberOfCars);
            return company;
        }

        private Car ReadCarFromFile(ref StreamReader streamReader, ref Company company) {
            int id = int.Parse(streamReader.ReadLine());
            string makeAndModel = streamReader.ReadLine();
            string registration = streamReader.ReadLine();
            string fuelType = streamReader.ReadLine();
            string lastServiceDate = streamReader.ReadLine();
            string comments = streamReader.ReadLine();

            Car car = new Car(id, makeAndModel, registration, fuelType, lastServiceDate, comments);
            return car;
        }

        private void EditCompanyDetails(Company company) {
            // MaskedTextBox.Mask handles input validation natively; any further validation is superfluous.
            int id = int.Parse(mtxCompanyId.Text);
            string name = txtName.Text;
            string address = txtAddress.Text;
            string postcode = txtPostcode.Text;
            int numberOfCars = int.Parse(mtxNumberOfCars.Text);

            company.Edit(id, name, address, postcode, numberOfCars);
        }

        private void EditCarDetails(Car car) {
            // MaskedTextBox.Mask handles input validation natively; any further validation is superfluous.
            int id = int.Parse(mtxCarId.Text);
            string makeAndModel = txtMakeAndModel.Text;
            string registration = txtRegistration.Text;
            string fuelType = txtFuelType.Text;
            string lastServiceDate = txtLastServiceDate.Text;
            string comments = rtfComments.Text;

            car.Edit(id, makeAndModel, registration, fuelType, lastServiceDate, comments);
        }

        private void DisplayCompanies(ArrayList companies, int index = 0) {
            lstCompanies.Items.Clear();

            foreach (Company company in companies) {
                lstCompanies.Items.Add(company.Name);
            }

            lstCompanies.SelectedIndex = index;
        }

        private void DisplayCars(ArrayList cars, int index = 0) {
            lstCars.Items.Clear();

            foreach (Car car in cars) {
                lstCars.Items.Add(car.Identity());
            }

            lstCars.SelectedIndex = index;
        }

        private void AddItemsToListbox(ListBox listBox, List<string> itemList) {
            // BUG: When lstCompanies search is changed, items in lstCars do not update.
            listBox.Items.Clear();

            foreach (string item in itemList) {
                listBox.Items.Add(item);
            }

            listBox.SelectedIndex = listBox.TopIndex;
        }

        private DialogResult ShowYesNoDialog(string message, string title) {
            DialogResult dialogResult = MessageBox.Show(message, title, MessageBoxButtons.YesNo);
            return dialogResult;
        }

        private bool AreCompanyDetailsFilled() {
            if (string.IsNullOrEmpty(mtxCompanyId.Text) || mtxCompanyId.MaskCompleted == false ||
                string.IsNullOrEmpty(txtName.Text) || string.IsNullOrEmpty(txtAddress.Text) ||
                 string.IsNullOrEmpty(txtPostcode.Text) || string.IsNullOrEmpty(mtxNumberOfCars.Text)) {
                return false;
            }

            return true;
        }

        private bool AreCarDetailsFilled() {
            if (string.IsNullOrEmpty(mtxCarId.Text) || mtxCarId.MaskCompleted == false ||
                string.IsNullOrEmpty(txtMakeAndModel.Text) || string.IsNullOrEmpty(txtRegistration.Text) ||
                string.IsNullOrEmpty(txtFuelType.Text) || string.IsNullOrEmpty(txtLastServiceDate.Text) ||
                string.IsNullOrEmpty(rtfComments.Text)) {
                return false;
            }

            return true;
        }

        private void EnableSavingIfDetailsAreFilled(KeyEventArgs e, Button saveButton) {
            if (saveButton == btnSaveCompanyDetails) {
                saveButton.Enabled = AreCompanyDetailsFilled();
            } else if (saveButton == btnSaveCarDetails) {
                saveButton.Enabled = AreCarDetailsFilled();
            }
        }

        private List<string> GetCompanySearchResults(ArrayList listOfCompanies, string searchQuery) {
            List<string> searchResults = new List<string>();

            foreach (Company company in listOfCompanies) {
                // https://stackoverflow.com/a/8879798
                if (company.Name.ToLower().Contains(searchQuery.ToLower())) {
                    searchResults.Add(company.Name);
                }
            }

            return searchResults;
        }

        private List<string> GetCarSearchResults(ArrayList listOfCars, string searchQuery) {
            List<string> searchResults = new List<string>();

            foreach (Car car in listOfCars) {
                // https://stackoverflow.com/a/8879798
                if (car.MakeAndModel.ToLower().Contains(searchQuery.ToLower()) ||
                    car.Id.ToString().Contains(searchQuery)) {
                    searchResults.Add(car.Identity());
                }
            }

            return searchResults;
        }

        private int GetSelectedCompanyIndex(ArrayList listOfCompanies, string nameOfCompanyToFind) {
            // Adapted from: https://stackoverflow.com/a/17995763
            Company company;
            int companyIndex = -1;

            for (int i = 0; i < listOfCompanies.Count; i++) {
                company = (Company)listOfCompanies[i];
                if (company.Name == nameOfCompanyToFind) {
                    companyIndex = i;
                    break;
                }
            }

            return companyIndex;
        }

        private int GetSelectedCarIndex(ArrayList listOfCars, int idOfCarToFind) {
            // Adapted from: https://stackoverflow.com/a/17995763
            Car car;
            int carIndex = -1;

            for (int i = 0; i < listOfCars.Count; i++) {
                car = (Car)listOfCars[i];
                if (car.Id == idOfCarToFind) {
                    carIndex = i;
                    break;
                }
            }

            return carIndex;
        }

        private Company GetSelectedCompany(ArrayList listOfCompanies, ListBox companyListBox) {
            Company selectedCompany;

            if (listOfCompanies.Count == companyListBox.Items.Count) {
                selectedCompany = (Company)listOfCompanies[companyListBox.SelectedIndex];
                // If the user has used the search box.
            } else {
                int selectedCompanyIndex = GetSelectedCompanyIndex(listOfCompanies, 
                    companyListBox.SelectedItem.ToString());
                selectedCompany = (Company)listOfCompanies[selectedCompanyIndex];
            }
            
            return selectedCompany;
        }

        private Car GetSelectedCar(ArrayList listOfCars, ListBox carListBox) {
            Car selectedCar;

            if (listOfCars.Count == carListBox.Items.Count) {
                selectedCar = (Car)listOfCars[carListBox.SelectedIndex];
            } else {
                // If the user has used the search box.
                int idOfCarToFind = GetSelectedCarId(carListBox.SelectedItem.ToString());
                int selectedCarIndex = GetSelectedCarIndex(listOfCars, idOfCarToFind);
                selectedCar = (Car)listOfCars[selectedCarIndex];
            }

            return selectedCar;
        }

        private int GetSelectedCarId(string selectedCarItem) {
            // Takes the SelectedItem of a ListBox (which contains the Identity() of a given car)
            // and splits the string using ':' as a separator.
            // The first string in the splitString array contains the Id of the car, which is used
            // in GetSelectedCarIndex().

            string[] splitString = selectedCarItem.Split(':');
            int carId = Convert.ToInt32(splitString[0]);
            return carId;
        }

        private void EmptyTextBoxText(Control.ControlCollection groupBoxControls) {
            foreach (Control control in groupBoxControls) {
                if (control is TextBox || control is MaskedTextBox || control is RichTextBox) {
                    control.Text = "";
                }
            }
        }

        private void ToggleGroupBoxControls(Control.ControlCollection groupBoxControls, bool toggle) {
            foreach (Control control in groupBoxControls) {
                control.Enabled = toggle;
            }
        }

        private void AddNewCompany() {
            Company company = new Company();
            companies.Add(company);
            EditCompanyDetails(company);

            WriteCompanyAndCarDataToFile(companies);
            RestoreFormControls();
            UpdateListBoxWithNewItem(lstCompanies, company.Name);

            MessageBox.Show("Company created successfully.");
            btnSaveCompanyDetails.Enabled = false;
            btnCancelCompanyCreation.Enabled = false;
        }

        private void EditExistingCompany(Company selectedCompany) {
            EditCompanyDetails(selectedCompany);
            WriteCompanyAndCarDataToFile(companies);
            lstCompanies.SelectedItem = selectedCompany.Name as object;
            
            // Re-draw the ListBox in case of a new Name.
            DisplayCompanies(companies, lstCompanies.SelectedIndex);
            string success = string.Format("{0} edited successfully.", selectedCompany.Name);
            MessageBox.Show(success);
            btnSaveCompanyDetails.Enabled = false;
        }

        private void AddNewCar(Company selectedCompany) {
            Car car = new Car();
            selectedCompany.Cars.Add(car);
            EditCarDetails(car);

            // WriteCompanyAndCarDataToFile() is dependent on using NumberOfCars in its for loop.
            // Therefore, it must be incremented here to work correctly.
            selectedCompany.NumberOfCars++;
            mtxNumberOfCars.Text = selectedCompany.NumberOfCars.ToString();

            WriteCompanyAndCarDataToFile(companies);
            RestoreFormControls();
            UpdateListBoxWithNewItem(lstCars, car.Identity());

            string success = string.Format("{0} added successfully.", car.MakeAndModel);
            MessageBox.Show(success);
            btnSaveCarDetails.Enabled = false;
            btnCancelCarCreation.Enabled = false;
        }

        private void EditExistingCar(Car selectedCar, Company selectedCompany) {
            EditCarDetails(selectedCar);
            WriteCompanyAndCarDataToFile(companies);
            lstCars.SelectedItem = selectedCar.Identity() as object;

            // Re-draw the ListBox in case of a new MakeAndModel.
            DisplayCars(selectedCompany.Cars, lstCars.SelectedIndex);
            string success = string.Format("{0} edited successfully.", selectedCar.MakeAndModel);
            MessageBox.Show(success);
            btnSaveCarDetails.Enabled = false;
        }

        private void RemoveCar(Company selectedCompany, int selectedCarIndex) {
            // WriteCompanyAndCarDataToFile() is dependent on using NumberOfCars in its for loop.
            // Therefore, it must be decreased to work correctly.
            selectedCompany.Cars.RemoveAt(selectedCarIndex);
            selectedCompany.NumberOfCars--;
            WriteCompanyAndCarDataToFile(companies);

            DisplayCars(selectedCompany.Cars);
            MessageBox.Show("Car removed successfully.");
        }

        private void PrepareFormForCompanyCreation() {
            // Disallow SelectedIndexChanged events to fire.
            updateDisplay = false;
            EmptyTextBoxText(grpCompanyDetails.Controls);

            // Disable controls outside of the active GroupBox.
            ToggleGroupBoxControls(grpCompanies.Controls, false);
            ToggleGroupBoxControls(grpCars.Controls, false);
            ToggleGroupBoxControls(grpCarDetails.Controls, false);

            // EditCompanyDetails() parses the value of mtxNumberOfCars.Text to set NumberOfCars.
            // As the user cannot edit the value of the textbox, its initial value is set here.
            mtxNumberOfCars.Text = "0";
            btnCancelCompanyCreation.Enabled = true;
            mtxCompanyId.Focus();
        }

        private void PrepareFormForCarCreation() {
            // Disallow SelectedIndexChanged events to fire.
            updateDisplay = false;
            EmptyTextBoxText(grpCarDetails.Controls);

            // Disable controls outside of the active GroupBox.
            ToggleGroupBoxControls(grpCompanies.Controls, false);
            ToggleGroupBoxControls(grpCompanyDetails.Controls, false);
            ToggleGroupBoxControls(grpCars.Controls, false);

            btnCancelCarCreation.Enabled = true;
            mtxCarId.Focus();
        }

        private void RestoreFormControls() {
            foreach (GroupBox groupBox in this.Controls) {
                ToggleGroupBoxControls(groupBox.Controls, true);
            }

            // mtxNumberOfCars should always be disabled.
            mtxNumberOfCars.Enabled = false;
            updateDisplay = true;
        }

        private void CancelObjectCreation(Button cancelButton) {
            string objectType = (cancelButton == btnCancelCompanyCreation) ? "company" : "car";
            string title = string.Format("Cancel {0} creation", objectType);
            DialogResult cancelObjectCreation = ShowYesNoDialog("Are you sure you want to cancel?", title);

            if (cancelObjectCreation == DialogResult.Yes) {
                RestoreFormControls();
                cancelButton.Enabled = false;
                DisplayCompanies(companies);
            }
        }

        private void UpdateListBoxWithNewItem(ListBox listBox, string item) {
            listBox.Items.Add(item);
            listBox.SelectedIndex = listBox.Items.IndexOf(item);
        }
    }
}
