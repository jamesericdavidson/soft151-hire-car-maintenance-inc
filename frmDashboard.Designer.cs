﻿namespace Hire_Car_Maintenance_Inc {
    partial class frmDashboard {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.grpCompanies = new System.Windows.Forms.GroupBox();
            this.btnAddCompany = new System.Windows.Forms.Button();
            this.lstCompanies = new System.Windows.Forms.ListBox();
            this.lblSearchCompanies = new System.Windows.Forms.Label();
            this.txtSearchCompanies = new System.Windows.Forms.TextBox();
            this.grpCompanyDetails = new System.Windows.Forms.GroupBox();
            this.btnCancelCompanyCreation = new System.Windows.Forms.Button();
            this.mtxNumberOfCars = new System.Windows.Forms.MaskedTextBox();
            this.mtxCompanyId = new System.Windows.Forms.MaskedTextBox();
            this.btnSaveCompanyDetails = new System.Windows.Forms.Button();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblNumberOfCars = new System.Windows.Forms.Label();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblCompanyId = new System.Windows.Forms.Label();
            this.grpCars = new System.Windows.Forms.GroupBox();
            this.btnRemoveCar = new System.Windows.Forms.Button();
            this.btnAddCar = new System.Windows.Forms.Button();
            this.lstCars = new System.Windows.Forms.ListBox();
            this.txtSearchCars = new System.Windows.Forms.TextBox();
            this.lblSearchCars = new System.Windows.Forms.Label();
            this.grpCarDetails = new System.Windows.Forms.GroupBox();
            this.btnCancelCarCreation = new System.Windows.Forms.Button();
            this.mtxCarId = new System.Windows.Forms.MaskedTextBox();
            this.btnSaveCarDetails = new System.Windows.Forms.Button();
            this.rtfComments = new System.Windows.Forms.RichTextBox();
            this.txtFuelType = new System.Windows.Forms.TextBox();
            this.txtLastServiceDate = new System.Windows.Forms.TextBox();
            this.txtRegistration = new System.Windows.Forms.TextBox();
            this.txtMakeAndModel = new System.Windows.Forms.TextBox();
            this.lblComments = new System.Windows.Forms.Label();
            this.lblLastServiceDate = new System.Windows.Forms.Label();
            this.lblFuelType = new System.Windows.Forms.Label();
            this.lblRegistration = new System.Windows.Forms.Label();
            this.lblMakeAndModel = new System.Windows.Forms.Label();
            this.lblCarId = new System.Windows.Forms.Label();
            this.tipInput = new System.Windows.Forms.ToolTip(this.components);
            this.grpCompanies.SuspendLayout();
            this.grpCompanyDetails.SuspendLayout();
            this.grpCars.SuspendLayout();
            this.grpCarDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpCompanies
            // 
            this.grpCompanies.Controls.Add(this.btnAddCompany);
            this.grpCompanies.Controls.Add(this.lstCompanies);
            this.grpCompanies.Controls.Add(this.lblSearchCompanies);
            this.grpCompanies.Controls.Add(this.txtSearchCompanies);
            this.grpCompanies.Location = new System.Drawing.Point(13, 13);
            this.grpCompanies.Name = "grpCompanies";
            this.grpCompanies.Size = new System.Drawing.Size(509, 370);
            this.grpCompanies.TabIndex = 0;
            this.grpCompanies.TabStop = false;
            this.grpCompanies.Text = "Companies";
            // 
            // btnAddCompany
            // 
            this.btnAddCompany.Location = new System.Drawing.Point(385, 104);
            this.btnAddCompany.Name = "btnAddCompany";
            this.btnAddCompany.Size = new System.Drawing.Size(58, 45);
            this.btnAddCompany.TabIndex = 3;
            this.btnAddCompany.Text = "+";
            this.btnAddCompany.UseVisualStyleBackColor = true;
            this.btnAddCompany.Click += new System.EventHandler(this.btnAddCompany_Click);
            // 
            // lstCompanies
            // 
            this.lstCompanies.FormattingEnabled = true;
            this.lstCompanies.ItemHeight = 29;
            this.lstCompanies.Location = new System.Drawing.Point(12, 78);
            this.lstCompanies.Name = "lstCompanies";
            this.lstCompanies.Size = new System.Drawing.Size(367, 265);
            this.lstCompanies.TabIndex = 2;
            this.lstCompanies.SelectedIndexChanged += new System.EventHandler(this.lstCompanies_SelectedIndexChanged);
            // 
            // lblSearchCompanies
            // 
            this.lblSearchCompanies.AutoSize = true;
            this.lblSearchCompanies.Location = new System.Drawing.Point(64, 40);
            this.lblSearchCompanies.Name = "lblSearchCompanies";
            this.lblSearchCompanies.Size = new System.Drawing.Size(89, 29);
            this.lblSearchCompanies.TabIndex = 0;
            this.lblSearchCompanies.Text = "Search";
            // 
            // txtSearchCompanies
            // 
            this.txtSearchCompanies.Location = new System.Drawing.Point(159, 37);
            this.txtSearchCompanies.Name = "txtSearchCompanies";
            this.txtSearchCompanies.Size = new System.Drawing.Size(220, 35);
            this.txtSearchCompanies.TabIndex = 1;
            this.txtSearchCompanies.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchCompanies_KeyUp);
            // 
            // grpCompanyDetails
            // 
            this.grpCompanyDetails.Controls.Add(this.btnCancelCompanyCreation);
            this.grpCompanyDetails.Controls.Add(this.mtxNumberOfCars);
            this.grpCompanyDetails.Controls.Add(this.mtxCompanyId);
            this.grpCompanyDetails.Controls.Add(this.btnSaveCompanyDetails);
            this.grpCompanyDetails.Controls.Add(this.txtPostcode);
            this.grpCompanyDetails.Controls.Add(this.txtName);
            this.grpCompanyDetails.Controls.Add(this.txtAddress);
            this.grpCompanyDetails.Controls.Add(this.lblNumberOfCars);
            this.grpCompanyDetails.Controls.Add(this.lblPostcode);
            this.grpCompanyDetails.Controls.Add(this.lblAddress);
            this.grpCompanyDetails.Controls.Add(this.lblName);
            this.grpCompanyDetails.Controls.Add(this.lblCompanyId);
            this.grpCompanyDetails.Location = new System.Drawing.Point(610, 13);
            this.grpCompanyDetails.Name = "grpCompanyDetails";
            this.grpCompanyDetails.Size = new System.Drawing.Size(653, 370);
            this.grpCompanyDetails.TabIndex = 1;
            this.grpCompanyDetails.TabStop = false;
            this.grpCompanyDetails.Text = "Company Details";
            // 
            // btnCancelCompanyCreation
            // 
            this.btnCancelCompanyCreation.Enabled = false;
            this.btnCancelCompanyCreation.Location = new System.Drawing.Point(349, 297);
            this.btnCancelCompanyCreation.Name = "btnCancelCompanyCreation";
            this.btnCancelCompanyCreation.Size = new System.Drawing.Size(106, 45);
            this.btnCancelCompanyCreation.TabIndex = 11;
            this.btnCancelCompanyCreation.Text = "Cancel";
            this.btnCancelCompanyCreation.UseVisualStyleBackColor = true;
            this.btnCancelCompanyCreation.Click += new System.EventHandler(this.btnCancelCompanyCreation_Click);
            // 
            // mtxNumberOfCars
            // 
            this.mtxNumberOfCars.Enabled = false;
            this.mtxNumberOfCars.Location = new System.Drawing.Point(189, 240);
            this.mtxNumberOfCars.Mask = "09";
            this.mtxNumberOfCars.Name = "mtxNumberOfCars";
            this.mtxNumberOfCars.Size = new System.Drawing.Size(36, 35);
            this.mtxNumberOfCars.TabIndex = 9;
            this.mtxNumberOfCars.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCompanyId_KeyUp);
            // 
            // mtxCompanyId
            // 
            this.mtxCompanyId.Location = new System.Drawing.Point(189, 48);
            this.mtxCompanyId.Mask = "00009";
            this.mtxCompanyId.Name = "mtxCompanyId";
            this.mtxCompanyId.Size = new System.Drawing.Size(78, 35);
            this.mtxCompanyId.TabIndex = 1;
            this.tipInput.SetToolTip(this.mtxCompanyId, "Please enter four or five numbers.");
            this.mtxCompanyId.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCompanyId_KeyUp);
            // 
            // btnSaveCompanyDetails
            // 
            this.btnSaveCompanyDetails.Enabled = false;
            this.btnSaveCompanyDetails.Location = new System.Drawing.Point(181, 297);
            this.btnSaveCompanyDetails.Name = "btnSaveCompanyDetails";
            this.btnSaveCompanyDetails.Size = new System.Drawing.Size(106, 45);
            this.btnSaveCompanyDetails.TabIndex = 10;
            this.btnSaveCompanyDetails.Text = "Save";
            this.btnSaveCompanyDetails.UseVisualStyleBackColor = true;
            this.btnSaveCompanyDetails.Click += new System.EventHandler(this.btnSaveCompanyDetails_Click);
            // 
            // txtPostcode
            // 
            this.txtPostcode.Location = new System.Drawing.Point(189, 189);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.Size = new System.Drawing.Size(155, 35);
            this.txtPostcode.TabIndex = 7;
            this.txtPostcode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCompanyId_KeyUp);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(188, 92);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(267, 35);
            this.txtName.TabIndex = 3;
            this.txtName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCompanyId_KeyUp);
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(188, 139);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(446, 35);
            this.txtAddress.TabIndex = 5;
            this.txtAddress.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCompanyId_KeyUp);
            // 
            // lblNumberOfCars
            // 
            this.lblNumberOfCars.AutoSize = true;
            this.lblNumberOfCars.Location = new System.Drawing.Point(6, 243);
            this.lblNumberOfCars.Name = "lblNumberOfCars";
            this.lblNumberOfCars.Size = new System.Drawing.Size(177, 29);
            this.lblNumberOfCars.TabIndex = 8;
            this.lblNumberOfCars.Text = "Number of cars";
            // 
            // lblPostcode
            // 
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(68, 192);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(115, 29);
            this.lblPostcode.TabIndex = 6;
            this.lblPostcode.Text = "Postcode";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(80, 142);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(102, 29);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "Address";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(104, 95);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(78, 29);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name";
            // 
            // lblCompanyId
            // 
            this.lblCompanyId.AutoSize = true;
            this.lblCompanyId.Location = new System.Drawing.Point(147, 51);
            this.lblCompanyId.Name = "lblCompanyId";
            this.lblCompanyId.Size = new System.Drawing.Size(36, 29);
            this.lblCompanyId.TabIndex = 0;
            this.lblCompanyId.Text = "ID";
            // 
            // grpCars
            // 
            this.grpCars.Controls.Add(this.btnRemoveCar);
            this.grpCars.Controls.Add(this.btnAddCar);
            this.grpCars.Controls.Add(this.lstCars);
            this.grpCars.Controls.Add(this.txtSearchCars);
            this.grpCars.Controls.Add(this.lblSearchCars);
            this.grpCars.Location = new System.Drawing.Point(13, 398);
            this.grpCars.Name = "grpCars";
            this.grpCars.Size = new System.Drawing.Size(509, 358);
            this.grpCars.TabIndex = 2;
            this.grpCars.TabStop = false;
            this.grpCars.Text = "Cars";
            // 
            // btnRemoveCar
            // 
            this.btnRemoveCar.Location = new System.Drawing.Point(385, 261);
            this.btnRemoveCar.Name = "btnRemoveCar";
            this.btnRemoveCar.Size = new System.Drawing.Size(58, 45);
            this.btnRemoveCar.TabIndex = 4;
            this.btnRemoveCar.Text = "-";
            this.btnRemoveCar.UseVisualStyleBackColor = true;
            this.btnRemoveCar.Click += new System.EventHandler(this.btnRemoveCar_Click);
            // 
            // btnAddCar
            // 
            this.btnAddCar.Location = new System.Drawing.Point(385, 115);
            this.btnAddCar.Name = "btnAddCar";
            this.btnAddCar.Size = new System.Drawing.Size(58, 45);
            this.btnAddCar.TabIndex = 3;
            this.btnAddCar.Text = "+";
            this.btnAddCar.UseVisualStyleBackColor = true;
            this.btnAddCar.Click += new System.EventHandler(this.btnAddCar_Click);
            // 
            // lstCars
            // 
            this.lstCars.FormattingEnabled = true;
            this.lstCars.ItemHeight = 29;
            this.lstCars.Location = new System.Drawing.Point(12, 93);
            this.lstCars.Name = "lstCars";
            this.lstCars.Size = new System.Drawing.Size(367, 236);
            this.lstCars.TabIndex = 2;
            this.lstCars.SelectedIndexChanged += new System.EventHandler(this.lstCars_SelectedIndexChanged);
            // 
            // txtSearchCars
            // 
            this.txtSearchCars.Location = new System.Drawing.Point(159, 52);
            this.txtSearchCars.Name = "txtSearchCars";
            this.txtSearchCars.Size = new System.Drawing.Size(220, 35);
            this.txtSearchCars.TabIndex = 1;
            this.txtSearchCars.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchCars_KeyUp);
            // 
            // lblSearchCars
            // 
            this.lblSearchCars.AutoSize = true;
            this.lblSearchCars.Location = new System.Drawing.Point(64, 55);
            this.lblSearchCars.Name = "lblSearchCars";
            this.lblSearchCars.Size = new System.Drawing.Size(89, 29);
            this.lblSearchCars.TabIndex = 0;
            this.lblSearchCars.Text = "Search";
            // 
            // grpCarDetails
            // 
            this.grpCarDetails.Controls.Add(this.btnCancelCarCreation);
            this.grpCarDetails.Controls.Add(this.mtxCarId);
            this.grpCarDetails.Controls.Add(this.btnSaveCarDetails);
            this.grpCarDetails.Controls.Add(this.rtfComments);
            this.grpCarDetails.Controls.Add(this.txtFuelType);
            this.grpCarDetails.Controls.Add(this.txtLastServiceDate);
            this.grpCarDetails.Controls.Add(this.txtRegistration);
            this.grpCarDetails.Controls.Add(this.txtMakeAndModel);
            this.grpCarDetails.Controls.Add(this.lblComments);
            this.grpCarDetails.Controls.Add(this.lblLastServiceDate);
            this.grpCarDetails.Controls.Add(this.lblFuelType);
            this.grpCarDetails.Controls.Add(this.lblRegistration);
            this.grpCarDetails.Controls.Add(this.lblMakeAndModel);
            this.grpCarDetails.Controls.Add(this.lblCarId);
            this.grpCarDetails.Location = new System.Drawing.Point(610, 398);
            this.grpCarDetails.Name = "grpCarDetails";
            this.grpCarDetails.Size = new System.Drawing.Size(653, 358);
            this.grpCarDetails.TabIndex = 3;
            this.grpCarDetails.TabStop = false;
            this.grpCarDetails.Text = "Car Details";
            // 
            // btnCancelCarCreation
            // 
            this.btnCancelCarCreation.Enabled = false;
            this.btnCancelCarCreation.Location = new System.Drawing.Point(349, 295);
            this.btnCancelCarCreation.Name = "btnCancelCarCreation";
            this.btnCancelCarCreation.Size = new System.Drawing.Size(106, 45);
            this.btnCancelCarCreation.TabIndex = 12;
            this.btnCancelCarCreation.Text = "Cancel";
            this.btnCancelCarCreation.UseVisualStyleBackColor = true;
            this.btnCancelCarCreation.Click += new System.EventHandler(this.btnCancelCarCreation_Click);
            // 
            // mtxCarId
            // 
            this.mtxCarId.Location = new System.Drawing.Point(212, 42);
            this.mtxCarId.Mask = "0000";
            this.mtxCarId.Name = "mtxCarId";
            this.mtxCarId.Size = new System.Drawing.Size(64, 35);
            this.mtxCarId.TabIndex = 1;
            this.tipInput.SetToolTip(this.mtxCarId, "Please enter four numbers.");
            this.mtxCarId.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCarId_KeyUp);
            // 
            // btnSaveCarDetails
            // 
            this.btnSaveCarDetails.Enabled = false;
            this.btnSaveCarDetails.Location = new System.Drawing.Point(181, 295);
            this.btnSaveCarDetails.Name = "btnSaveCarDetails";
            this.btnSaveCarDetails.Size = new System.Drawing.Size(106, 45);
            this.btnSaveCarDetails.TabIndex = 11;
            this.btnSaveCarDetails.Text = "Save";
            this.btnSaveCarDetails.UseVisualStyleBackColor = true;
            this.btnSaveCarDetails.Click += new System.EventHandler(this.btnSaveCarDetails_Click);
            // 
            // rtfComments
            // 
            this.rtfComments.Location = new System.Drawing.Point(434, 90);
            this.rtfComments.Name = "rtfComments";
            this.rtfComments.Size = new System.Drawing.Size(200, 182);
            this.rtfComments.TabIndex = 10;
            this.rtfComments.Text = "";
            this.rtfComments.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCarId_KeyUp);
            // 
            // txtFuelType
            // 
            this.txtFuelType.Location = new System.Drawing.Point(212, 187);
            this.txtFuelType.Name = "txtFuelType";
            this.txtFuelType.Size = new System.Drawing.Size(132, 35);
            this.txtFuelType.TabIndex = 6;
            this.txtFuelType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCarId_KeyUp);
            // 
            // txtLastServiceDate
            // 
            this.txtLastServiceDate.Location = new System.Drawing.Point(212, 237);
            this.txtLastServiceDate.Name = "txtLastServiceDate";
            this.txtLastServiceDate.Size = new System.Drawing.Size(132, 35);
            this.txtLastServiceDate.TabIndex = 8;
            this.txtLastServiceDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCarId_KeyUp);
            // 
            // txtRegistration
            // 
            this.txtRegistration.Location = new System.Drawing.Point(212, 140);
            this.txtRegistration.Name = "txtRegistration";
            this.txtRegistration.Size = new System.Drawing.Size(132, 35);
            this.txtRegistration.TabIndex = 4;
            this.txtRegistration.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCarId_KeyUp);
            // 
            // txtMakeAndModel
            // 
            this.txtMakeAndModel.Location = new System.Drawing.Point(212, 90);
            this.txtMakeAndModel.Name = "txtMakeAndModel";
            this.txtMakeAndModel.Size = new System.Drawing.Size(208, 35);
            this.txtMakeAndModel.TabIndex = 2;
            this.txtMakeAndModel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtxCarId_KeyUp);
            // 
            // lblComments
            // 
            this.lblComments.AutoSize = true;
            this.lblComments.Location = new System.Drawing.Point(473, 58);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(129, 29);
            this.lblComments.TabIndex = 9;
            this.lblComments.Text = "Comments";
            // 
            // lblLastServiceDate
            // 
            this.lblLastServiceDate.AutoSize = true;
            this.lblLastServiceDate.Location = new System.Drawing.Point(6, 240);
            this.lblLastServiceDate.Name = "lblLastServiceDate";
            this.lblLastServiceDate.Size = new System.Drawing.Size(200, 29);
            this.lblLastServiceDate.TabIndex = 7;
            this.lblLastServiceDate.Text = "Last Service Date";
            // 
            // lblFuelType
            // 
            this.lblFuelType.AutoSize = true;
            this.lblFuelType.Location = new System.Drawing.Point(84, 190);
            this.lblFuelType.Name = "lblFuelType";
            this.lblFuelType.Size = new System.Drawing.Size(122, 29);
            this.lblFuelType.TabIndex = 5;
            this.lblFuelType.Text = "Fuel Type";
            // 
            // lblRegistration
            // 
            this.lblRegistration.AutoSize = true;
            this.lblRegistration.Location = new System.Drawing.Point(64, 143);
            this.lblRegistration.Name = "lblRegistration";
            this.lblRegistration.Size = new System.Drawing.Size(142, 29);
            this.lblRegistration.TabIndex = 3;
            this.lblRegistration.Text = "Registration";
            // 
            // lblMakeAndModel
            // 
            this.lblMakeAndModel.AutoSize = true;
            this.lblMakeAndModel.Location = new System.Drawing.Point(14, 93);
            this.lblMakeAndModel.Name = "lblMakeAndModel";
            this.lblMakeAndModel.Size = new System.Drawing.Size(192, 29);
            this.lblMakeAndModel.TabIndex = 1;
            this.lblMakeAndModel.Text = "Make and Model";
            // 
            // lblCarId
            // 
            this.lblCarId.AutoSize = true;
            this.lblCarId.Location = new System.Drawing.Point(170, 45);
            this.lblCarId.Name = "lblCarId";
            this.lblCarId.Size = new System.Drawing.Size(36, 29);
            this.lblCarId.TabIndex = 0;
            this.lblCarId.Text = "ID";
            // 
            // frmDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 776);
            this.Controls.Add(this.grpCarDetails);
            this.Controls.Add(this.grpCars);
            this.Controls.Add(this.grpCompanyDetails);
            this.Controls.Add(this.grpCompanies);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "frmDashboard";
            this.Text = "Hire Car Maintenance Dashboard";
            this.Load += new System.EventHandler(this.frmDashboard_Load);
            this.grpCompanies.ResumeLayout(false);
            this.grpCompanies.PerformLayout();
            this.grpCompanyDetails.ResumeLayout(false);
            this.grpCompanyDetails.PerformLayout();
            this.grpCars.ResumeLayout(false);
            this.grpCars.PerformLayout();
            this.grpCarDetails.ResumeLayout(false);
            this.grpCarDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpCompanies;
        private System.Windows.Forms.ListBox lstCompanies;
        private System.Windows.Forms.Label lblSearchCompanies;
        private System.Windows.Forms.TextBox txtSearchCompanies;
        private System.Windows.Forms.Button btnAddCompany;
        private System.Windows.Forms.GroupBox grpCompanyDetails;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblCompanyId;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblNumberOfCars;
        private System.Windows.Forms.Label lblPostcode;
        private System.Windows.Forms.TextBox txtPostcode;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Button btnSaveCompanyDetails;
        private System.Windows.Forms.GroupBox grpCars;
        private System.Windows.Forms.TextBox txtSearchCars;
        private System.Windows.Forms.Label lblSearchCars;
        private System.Windows.Forms.Button btnRemoveCar;
        private System.Windows.Forms.Button btnAddCar;
        private System.Windows.Forms.ListBox lstCars;
        private System.Windows.Forms.GroupBox grpCarDetails;
        private System.Windows.Forms.Label lblCarId;
        private System.Windows.Forms.Label lblMakeAndModel;
        private System.Windows.Forms.Label lblComments;
        private System.Windows.Forms.Label lblLastServiceDate;
        private System.Windows.Forms.Label lblFuelType;
        private System.Windows.Forms.Label lblRegistration;
        private System.Windows.Forms.RichTextBox rtfComments;
        private System.Windows.Forms.TextBox txtFuelType;
        private System.Windows.Forms.TextBox txtLastServiceDate;
        private System.Windows.Forms.TextBox txtRegistration;
        private System.Windows.Forms.TextBox txtMakeAndModel;
        private System.Windows.Forms.Button btnSaveCarDetails;
        private System.Windows.Forms.MaskedTextBox mtxCompanyId;
        private System.Windows.Forms.MaskedTextBox mtxNumberOfCars;
        private System.Windows.Forms.MaskedTextBox mtxCarId;
        private System.Windows.Forms.Button btnCancelCompanyCreation;
        private System.Windows.Forms.ToolTip tipInput;
        private System.Windows.Forms.Button btnCancelCarCreation;
    }
}