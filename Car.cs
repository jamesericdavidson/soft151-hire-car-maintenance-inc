﻿/*
 * Copyright 2017, 2018 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hire_Car_Maintenance_Inc {
    public class Car {
        // Variables.

        private int id;
        private string makeAndModel;
        private string registration;
        private string fuelType;
        private string lastServiceDate;
        private string comments;

        // Properties.

        public int Id {
            get {
                return id;
            }
            set {
                id = value;
            }
        }

        public string MakeAndModel {
            get {
                return makeAndModel;
            }
            set {
                makeAndModel = value;
            }
        }

        public string Registration {
            get {
                return registration;
            }
            set {
                registration = value;
            }
        }

        public string FuelType {
            get {
                return fuelType;
            }
            set {
                fuelType = value;
            }
        }

        public string LastServiceDate {
            get {
                return lastServiceDate;
            }
            set {
                lastServiceDate = value;
            }
        }

        public string Comments {
            get {
                return comments;
            }
            set {
                comments = value;
            }
        }

        // Constructors.

        public Car() {

        }

        public Car(int id, string makeAndModel, string registration, string fuelType, string lastServiceDate,
            string comments) {
            Id = id;
            MakeAndModel = makeAndModel;
            Registration = registration;
            FuelType = fuelType;
            LastServiceDate = lastServiceDate;
            Comments = comments;
        }

        // Methods.

        public string Identity() {
            // TODO: Rename method to something more descriptive.
            string identity = string.Format("{0}: {1}", id.ToString(), MakeAndModel);
            return identity;
        }

        public string Information() {
            // When writing back to a file, it will always terminate with a null line (as interpreted by StreamReader)
            // As null is a condition for EndOfStream, this will not affect the application's logic and 
            // does not need to be changed.

            string output = string.Format("{0}" + Environment.NewLine + "{1}" + Environment.NewLine
                + "{2}" + Environment.NewLine + "{3}" + Environment.NewLine + "{4}" + Environment.NewLine
                + "{5}" + Environment.NewLine, Id.ToString(), MakeAndModel, Registration, FuelType, 
                LastServiceDate, Comments);
            return output;
        }

        public void Edit(int id, string makeAndModel, string registration, string fuelType, string lastServiceDate,
            string comments) {
            Id = id;
            MakeAndModel = makeAndModel;
            Registration = registration;
            FuelType = fuelType;
            LastServiceDate = lastServiceDate;
            Comments = comments;
        }
    }
}
