﻿/*
 * Copyright 2017, 2018 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Hire_Car_Maintenance_Inc {
    public class Company {
        // Variables.

        private int id;
        private string name;
        private string address;
        private string postcode;
        private int numberOfCars;
        private ArrayList cars = new ArrayList();

        // Properties.

        public int Id {
            get {
                return id;
            }
            set {
                id = value;
            }
        }

        public string Name {
            get {
                return name;
            }
            set {
                name = value;
            }
        }

        public string Address {
            get {
                return address;
            }
            set {
                address = value;
            }
        }

        public string Postcode {
            get {
                return postcode;
            }
            set {
                postcode = value;
            }
        }

        public int NumberOfCars {
            get {
                return numberOfCars;
            }
            set {
                numberOfCars = value;
            }
        }

        public ArrayList Cars {
            get {
                return cars;
            }
            set {
                cars = value;
            }
        }

        // Constructors.

        public Company() {

        }

        public Company(int id, string name, string address, string postcode, int numberOfCars) {
            Id = id;
            Name = name;
            Address = address;
            Postcode = postcode;
            NumberOfCars = numberOfCars;
        }

        // Methods.

        public string Information() {
            string output = string.Format("{0}" + Environment.NewLine + "{1}" + Environment.NewLine
                + "{2}" + Environment.NewLine + "{3}" + Environment.NewLine + "{4}"
                + Environment.NewLine, Id.ToString(), Name, Address, Postcode, NumberOfCars.ToString());
            return output;
        }

        /// <summary>
        /// Edit details of the company without affecting its existing Cars.
        /// </summary>

        public void Edit(int id, string name, string address, string postcode, int numberOfCars) {
            Id = id;
            Name = name;
            Address = address;
            Postcode = postcode;
            NumberOfCars = numberOfCars;
        }
    }
}
