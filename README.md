# Hire Car Maintenance Inc

Coursework for SOFT151 - Introduction to Object-oriented Programming

*A GUI application for hire car companies*

![Screenshot demonstration of Hire Car Maintenance Inc](normal.png)

![Screenshot demonstration of the search bar functionality in Hire Car Maintenance Inc](search.png)

An executable and input file are provided in `demo` for demonstration purposes. It is compatible with .NET Framework and Mono.

---

```
Copyright 2017, 2018 James Davidson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
